from random import randint
import hashlib

def generate_random_users(User_count):
	generated_users = {}	
	with open("generated_users_plain_text", "w") as outfile:
		for usercount in range(User_count):
			random_int = randint(0, len(dictionary_entries) - 1)
			dict_entry = dictionary_entries[random_int]

			choice_hash = randint(0, 2)
			if choice_hash == 0:
				hashed_entry = hashlib.md5(dict_entry).hexdigest()
			elif choice_hash == 1:
				hashed_entry = hashlib.sha1(dict_entry).hexdigest()
			elif choice_hash == 2:
				hashed_entry = hashlib.sha256(dict_entry).hexdigest()

			user_name = "user_" + str(usercount).zfill(4)
			generated_users[user_name] = hashed_entry
			outfile.write(user_name + "," + dict_entry + "\n")
	return generated_users

def generate_sequential():
	generated_users = {}
	with open("generated_users_plain_text", "w") as outfile:
		for usercount, dict_entry in enumerate(dictionary_entries):
			hashed_entry = hashlib.md5(dict_entry).hexdigest()
			user_name = "user_" + str(usercount).zfill(4)
			generated_users[user_name] = hashed_entry
			outfile.write(user_name + "," + dict_entry + "\n")		
	return generated_users

def read_dictionary(dict_name):
	dictionary_entries = []
	with open(dict_name, "r") as dic:
		for entry in dic:
			dictionary_entries.append(entry[:-1])
	return dictionary_entries

def write_to_file(generated_users):
	with open("generated_users", "w") as outfile:
		for key in generated_users.keys():
			outfile.write(key + "," + generated_users[key] + "\n")

dictionary_entries = read_dictionary("dictionary")
generated_users =  generate_random_users(1000)
write_to_file(generated_users)