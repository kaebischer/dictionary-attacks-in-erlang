#!/bin/bash
echo "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n"
echo "Welcome to the teDA deployment script for Distributed Dictionary Attacks !\n"
echo "Please enter your username:"
read username
echo "Please enter which Hashing method you would use (md5, sha1, sha256, all):"
read hash_method
echo "\n"
echo "Running..."

../../scripts/ping.sh project hosts.conf
../../scripts/depl_app.sh project make
../../scripts/depl_enodes.sh project hosts_alive.conf
line=$(head -1 hosts_alive.conf)
host_master=$(echo $line | cut -d' ' -f1)
../../scripts/run.sh project "master:start($hash_method,0,teda)" hosts_alive.conf $host_master $username

echo "\n"
echo "Please enter your password for copying outputs from remote machines"
rm -r -f teda_out/*
while IFS='' read -r host_line || [[ -n "$host_line" ]]; do
    host="$(echo $host_line | cut -d' ' -f1)"
    scp -r $username@$host:mpe/erl/teda/apps/project/out teda_out/$host
    echo "\n"
done < hosts_alive.conf

echo "Cleaning remote machines..."
../../scripts/depl_app.sh project clean
echo "\n"
echo "It's finished, thank you for using this script !"