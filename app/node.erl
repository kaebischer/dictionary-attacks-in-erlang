-module(node).
-export([start/5, spawn_subprocess/4]).

%% @doc provides a logging functionality. Takes the same arguments as io:format. If the atom false is provided, logs are only writtent to the .hrl file and not shown in the terminal
log(String, List) -> 
	Filename = "out/nodes/node" ++ pid_to_list(self()) ++ ".hrl",
	file:write_file(Filename, io_lib:fwrite(String, List), [append]).

start(Dictionary, Hash_Function, Process_count, Pid_Master, All_positions) -> 
	log("[~p] Started~n    Dict:~p~n", [self(), Dictionary]),

	TS = tuple_space:initialize_tuple_space(),
	random:seed(now()),
	Random_choice = random:uniform(4),
	log("Random_choice : ~p~n", [Random_choice]),
	case Random_choice =:= 4 of
		true -> log("Crash~n", []),
				io:format("[Node ~p] Crashed~n", [self()]),
				exit(exit);
		false -> def
	end,

	Temp = length(Dictionary) / Process_count,
	Subdict_length = utils:ceiling(Temp),

	Dict_Segments = segment_dictionary(Dictionary, Subdict_length),
	Subprocesses = spawn_subprocesses(Dict_Segments, Hash_Function, 0, TS),
	receive_loop(length(Dict_Segments), Subprocesses, Dict_Segments, Hash_Function, Pid_Master, All_positions, TS, 1).

%% @doc enter receiving state, where results are handled and set to the master
receive_loop(Len, Subprocesses, Dict_Segments, Hash_Function, Pid_Master, All_positions, TS, Timeout_Multiplier) ->
	{Positions, Results, New_timeout} = reception(Len, Subprocesses, Dict_Segments, Hash_Function, Pid_Master, All_positions, TS, Timeout_Multiplier),
	log("[Node ~p] Sends Results to master: ~p~n", [self(), Results]),
	Compressed_Positions = utils:compress(Positions),
	Pid_Master ! {self(), Results, Compressed_Positions, results},
	Updated_TS = request_TS(Pid_Master),
	receive_loop(-1, [], [], Hash_Function, Pid_Master, [], Updated_TS, New_timeout).

%% @doc spawns a subprocess for each provided dictionary segment
spawn_subprocesses([], _Hash_Function, _, _) -> 
	[];
spawn_subprocesses([Head | Tail], Hash_Function, Position, TS) ->
	Ret = {spawn(subprocess, start, [Head, Hash_Function, self(), TS]), Position},
	{Temp, _} = Ret,
	log("[Node~p], spawned sub ~p with position ~p and dict ~p~n", [self(), Temp, Position, Head]),
	[Ret | spawn_subprocesses(Tail, Hash_Function, Position + 1, TS)].

%% @doc cut the provided dictionary to segments of length N
segment_dictionary([], _) -> 
	[];
segment_dictionary(Dictionary, N) when length(Dictionary) >= N ->
	{Treated_Here, Passed_On} = lists:split(N, Dictionary),
	[Treated_Here | segment_dictionary(Passed_On, N)];
segment_dictionary(Dictionary, _) ->
	[Dictionary].

%% @doc handle the reception of results. Distinguishes between running subprocesses and zombie subprocesses
handle_result(false, Remaining_subpr, Dict_Segments, Hash_Function, {Pid, _}, Pid_Master, Previous_Positions, TS, Timeout_Multiplier) -> 
	log("[Node ~p] Received message from zombie process ~p~n", [self(),Pid]),
	reception(length(Remaining_subpr), Remaining_subpr, Dict_Segments, Hash_Function, Pid_Master, Previous_Positions, TS, Timeout_Multiplier*2);
handle_result(true, Remaining_subpr, Dict_Segments, Hash_Function, {Pid, Result_list}, Pid_Master, Previous_Positions, TS, Timeout_Multiplier) ->
	Remaining_subp_count = length(Remaining_subpr),
	log("[Node ~p] Accepted result from ~p. Remaining count before decrement: ~p~n", [self(), Pid, Remaining_subp_count]),
    Remaining_subprocesses_list_updated = utils:remove_enode(Pid, Remaining_subpr),
    {Res_pos, Res_list, New_timeout}  = reception(Remaining_subp_count - 1, Remaining_subprocesses_list_updated, Dict_Segments, Hash_Function, Pid_Master, Previous_Positions, TS, Timeout_Multiplier),
    {lists:usort(Previous_Positions ++ Res_pos), Result_list ++ Res_list, New_timeout}.

%% @doc handle reception of messages from subprocesses and the master
reception(0, _, _, _, _, _, _, TM) -> 
	{[], [], TM};
reception(Remaining_subp_count, Remaining_subpr, Dict_Segments, Hash_Function, Pid_Master, Previous_Positions, TS, Timeout_Multiplier) ->
	receive
		{new_task, NewDict, Positions} ->
			log("[Node ~p] Received new task with positions ~w~n", [self(), Positions]),

			Process_count = 2,
			Temp = length(NewDict) / Process_count,
			Subdict_length = utils:ceiling(Temp),

			New_Segmented_dictionary = segment_dictionary(NewDict, Subdict_length),
			Subprocesses = spawn_subprocesses(New_Segmented_dictionary, Hash_Function, length(Dict_Segments), TS),
			New_processes_count = length(Subprocesses),
			if
				Remaining_subp_count =:= -1 ->
					Basis = 0;
				true -> Basis = Remaining_subp_count
			end,
			log("[Node ~p] New Task with segment ~p. Remaining count after calc: ~p~n", [self(), NewDict, Basis + New_processes_count]),
			reception(Basis + New_processes_count, Remaining_subpr ++ Subprocesses, Dict_Segments ++ New_Segmented_dictionary, Hash_Function, Pid_Master, Previous_Positions ++ utils:decompress(Positions), TS, Timeout_Multiplier);

		{Pid, Result_list} ->			
			log("[Node ~p] Received Result_list ~p from ~p~n", [self(), Result_list, Pid]),
			Results = {Pid, Result_list},
			handle_result(utils:is_enode_in_list(Pid, Remaining_subpr), Remaining_subpr, Dict_Segments, Hash_Function, Results, Pid_Master, Previous_Positions, TS, Timeout_Multiplier);
		exit -> 
			log("[Node ~p] Received exit~n", [self()]),
			[Pid ! considered_dead || {Pid, _} <- Remaining_subpr],
			exit({[], [], Timeout_Multiplier});
		still_alive ->
			log("[Node ~p] Received still_alive~n", [self()]),
			Pid_Master ! {still_alive, self()},
			reception(Remaining_subp_count, Remaining_subpr, Dict_Segments, Hash_Function, Pid_Master, Previous_Positions, TS, Timeout_Multiplier);
		considered_dead ->
			log("[Node ~p] Received considered dead. Resets and waits for new segments~n", [self()]),
			[Pid ! considered_dead || {Pid, _} <- Remaining_subpr],
			TS ! reset,
			TS_new = request_TS(Pid_Master),
			reception(-1, [], [], Hash_Function, Pid_Master, [], TS_new, Timeout_Multiplier)

	after 20000 * Timeout_Multiplier ->
		if
			Remaining_subp_count =:= -1 ->
				reception(-1, Remaining_subpr, Dict_Segments, Hash_Function, Pid_Master, Previous_Positions, TS, Timeout_Multiplier);
			true ->
				log("[Node ~p] Not all suprocesses responded. Remaining: ~p,:~p ~n", [self(), Remaining_subp_count, Remaining_subpr]),
				TS ! reset,
				New_TS = request_TS(Pid_Master),
				New_processes = [spawn_subprocess(utils:elem(Position, Dict_Segments), Hash_Function, Position, New_TS) || {_, Position} <- Remaining_subpr],
				reception(Remaining_subp_count, New_processes, Dict_Segments, Hash_Function, Pid_Master, Previous_Positions, New_TS, Timeout_Multiplier)
		end
	end.

%% @doc spawn a subprocess with the provided dictionary segment
spawn_subprocess(Segment, Hash_Function, Position, TS) ->
	{spawn(subprocess, start, [Segment, Hash_Function, self(), TS]), Position}.

%% @doc sent a request for a new tuple space to the master
request_TS(Pid_Master) ->
	log("[Node ~p] Requesting new tuple space~n", [self()]),
	Pid_Master ! {self(), new_tuple_space_request},
	receive_TS(Pid_Master).

%% @doc listen for the reception of a new tuple space. Also handle still_alive pings in case of messages crossing
receive_TS(Pid_Master) ->
	receive
		{new_tuple_space, TS} -> 
			log("[Node ~p] Received new tuple_space ~n", [self()]),
			TS;
		still_alive -> Pid_Master ! {still_alive, self()},
			log("[Node ~p] Sent still_alive ~n", [self()]),
			receive_TS(Pid_Master)
	end.