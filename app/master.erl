-module(master).
-export([start/3, st/0, sv/0]).

%% @doc provides a logging functionality. Takes the same arguments as io:format. If the atom false is provided, logs are only writtent to the .hrl file and not shown in the terminal
log(String, List) -> 
	io:format(String, List),
	file:write_file("out/master.hrl", io_lib:fwrite(String, List), [append]).
log(String, List, false) ->
	file:write_file("out/master.hrl", io_lib:fwrite(String, List), [append]).

%% @doc starts de program with predefined default parameters
st() -> start(all, 0, teda).
sv() -> start(all, 0, virtual).

%% @doc Main function of the master node.
%% The master node will consult the dictionnary and will initialize enodes with their subdictionary and the Hash_Function chosen.
%% NumberOfEnodes is determined by the number of nodes available on TEDA.
start(Hash_Function, Debug_Mode, Deploy_Mode) when Hash_Function =:= none; Hash_Function =:= md5; Hash_Function =:= sha1; Hash_Function =:= sha256; Hash_Function =:= all ->
	% utils:begin_statistics(),
	io:fwrite("Master started~n"),
	
	{ok, [_, _|Enodes]} = file:consult('enodes.conf'),	%---- read nodes id from enodes.conf produced by enodes.sh. First enode is ignored to avoid running a node on the master
	Enode_count = length(Enodes),					%---- NumberOfEnodes is use to know how many subdictionaries must be created
	
	Dictionary_In_List_Format = utils:read_file("dictionaries/dictionary"),
	Subdictionary = utils:split(Dictionary_In_List_Format, 1000),
	log("Master subdictionaries: ~p~n", [Subdictionary]),

	%% @doc Enodes_with_position has the structure {PID_of_new_enode, Position_in_dictionary} (the latter to indentify which subdictionary is being treated)}
	First_batch = utils:first_elements(Enode_count, Subdictionary),
	Enodes_with_position = initialize_enodes(First_batch, Enodes, Hash_Function, 0, Deploy_Mode),
	
	Last_sent = Enode_count * length(utils:elem(0, Subdictionary)) - 1,
	Last_Sent_Positions = {_Position_in_Segments, _Position_in_Dictionary} = {Enode_count - 1, Last_sent},
	
	Master_TS = tuple_space:initialize_tuple_space(),
	Results = receive_results(Enodes_with_position, Subdictionary, Hash_Function, Enodes_with_position, Last_Sent_Positions, 1, Master_TS),

	[log("[Master] sends exit to ~p~n", [Pid_node_masters]) || {Pid_node_masters, _} <- Enodes_with_position],
	[Pid_node_masters ! exit || {Pid_node_masters, _} <- Enodes_with_position],
	print_results(Debug_Mode, Results);
	
	% utils:end_statistics();
start(Hash_Function, _, _) ->
	log("--- ERROR from master(~p) : args values not allowed~n", [Hash_Function]).

%% @doc A function printing results based on Debug_Mode argument
print_results(Debug_Mode, Results) ->
	if
		Debug_Mode =:= 0 ->
			Flatten_Results = utils:flatten_results(lists:flatten(Results),[]);
		Debug_Mode =:= 1 ->
			Flatten_Results = utils:flatten_results_with_pid(lists:flatten(Results),[]);
		true ->
			Flatten_Results = utils:flatten_results(lists:flatten(Results),[])
	end,

	io:fwrite("~nResults :~n------------~n"),
	Final_Results = utils:sort_list(Flatten_Results),
	utils:pretty_print(Final_Results),
	log("Found ~p passwords~n", [length(Flatten_Results)]).

%% @doc A function creating as many subdictionaries as enodes available on TEDA.
create_subdicts(_, 0) -> log("[Master ~p] No nodes available anymore~n", [self()], false), [];
create_subdicts(Dict_segments, Enode_count) ->
	Line_count = length(Dict_segments),
	N = Line_count / Enode_count,
	Subditionary_length = utils:ceiling(N),
	utils:split(Dict_segments, Subditionary_length).

%% @doc A function spawning enodes available on TEDA with their subdictionary.
initialize_enodes([], _, _Hash_Function, _, _) -> [];
initialize_enodes(Dictionary, [Head_Enodes | Tail_Enodes], Hash_Function, Position, Deploy_Mode) ->
	[Head_Subdictionaries | Tail_Subdictionaries] = Dictionary,
	Positions = utils:range(Position, Position + length(Head_Subdictionaries)),
	if
		Deploy_Mode =:= teda ->
			Spawn = spawn(Head_Enodes, node, start, [Head_Subdictionaries, Hash_Function, 2, self(), Positions]); %---- spawn for TEDA
		Deploy_Mode =:= virtual ->
			Spawn = spawn(node, start, [Head_Subdictionaries, Hash_Function, 2, self(), Positions])
	end,
	[{Spawn, Positions} | initialize_enodes(Tail_Subdictionaries,Tail_Enodes, Hash_Function, Position + length(Head_Subdictionaries), Deploy_Mode)].

%% @doc Arguments:
% -List of enodes from which an answer is expected
% -Dictionary (segmented)
% -Hash function
% -Enodes which responded during the last cycle
receive_results(_, _, _, Enodes, _, _, _) when length(Enodes) =:= 0 ->
		log("[Master ~p] No nodes available anymore~n", [self()], false),
		[];
receive_results(Enodes_unknown, Dict_segments, Hash_Function, Enodes_all, Last_sent, Timeout_Multiplier, Master_TS) ->
	receive
		%% @doc reception of results from an enode
		{Pid, Result_list, CPositions, results} ->
			Results = {Pid, Result_list},
			Positions = utils:decompress(CPositions),
			log("[Master ~p] Received unchecked result of length ~p from ~p~n", [self(), length(Result_list), Pid], false),
			Is_result_accepted = utils:is_enode_in_list(Pid, Enodes_unknown),
			handle_result(Is_result_accepted, Enodes_unknown, Dict_segments, Hash_Function, Enodes_all, Results, Positions, Last_sent, Timeout_Multiplier, Master_TS);
		{Pid_node, new_tuple_space_request} ->
			log("[Master ~p] Received new tuple space request from ~p~n", [self(), Pid_node]),
			New_Tuple_Space = tuple_space:copy_TS(Master_TS),
			Pid_node ! {new_tuple_space, New_Tuple_Space},
			log("[Master ~p] Sent new tuple space ~p to ~p~n", [self(), New_Tuple_Space, Pid_node]),
			receive_results(Enodes_unknown, Dict_segments, Hash_Function, Enodes_all, Last_sent, Timeout_Multiplier, Master_TS)
	after 1000 * Timeout_Multiplier ->
		%% @doc every cycle, remove enodes which have not responded since the last cycle
		TS_Size = tuple_space:get_TS_size(Master_TS),
		if
			TS_Size =:= 0 ->
				log("[Master ~p] No passwords remaining to attack, send exit to running nodes~n", [self()]),
				[Pid ! exit || {Pid, _} <- Enodes_all];
			true -> ok
		end,

		Enodes_alive = ping(Enodes_all),

		Enode_zombies = [{Enode_id, Positions} || {Enode_id, Positions} <- Enodes_unknown, utils:id_not_in_tuple_list(Enode_id, Enodes_alive)],
		[Pid ! considered_dead || {Pid, _} <- Enode_zombies],
		
		Enodes_alive_cleared = remove_idle(Enodes_alive),

		Updated_responsive_enodes_list = distribute_subdicts(Enode_zombies, Dict_segments, Enodes_alive_cleared),
		log("[Master ~p] Updated responsive enodes list: ~w~n", [self(), Updated_responsive_enodes_list], false),
		receive_results(Updated_responsive_enodes_list, Dict_segments, Hash_Function, Enodes_alive, Last_sent, Timeout_Multiplier * 2, Master_TS)

	end.

%% @doc after timeout, perform check to verify that unresponsive enodes are really down
ping([]) -> [];
ping(Enodes_unknown) ->
	[log("[Master ~p] Send still alive ping to: ~p~n", [self(), Pid]) || {Pid, _} <- Enodes_unknown],
	T = [Pid ! still_alive || {Pid, _} <- Enodes_unknown],
	receive_ack(length(T), Enodes_unknown).

%% @doc wait for acknowledgment of still alive request
receive_ack(0, _) -> [];
receive_ack(Pos, Enodes_unknown) ->
	receive
		{still_alive, Pid} ->
			{_, Positions} = utils:get_enode(Pid, Enodes_unknown),
			[{Pid, Positions} | receive_ack(Pos - 1, Enodes_unknown)]
		after 10000 ->
			log("[Master ~p] Still alive timeout~n", [self()]),
			[]
	end.

%% @doc distribuite the subdictionaries of the dead enodes to the available enodes
distribute_subdicts([], _, Enodes) -> Enodes;
distribute_subdicts(Enode_zombies, Dict_segments, Enodes_alive) ->
	[Head_zombie | Tail_zombies] = Enode_zombies,
	{_, Lost_subdict_positions} = Head_zombie,
	Lost_subdict_entries = [utils:elem(Position, utils:flatten_once(Dict_segments))|| Position <- Lost_subdict_positions],
	Still_running_enodes_count = length(Enodes_alive),
	Split_lost_dictionary = create_subdicts(Lost_subdict_entries, Still_running_enodes_count),
	
	Temp = create_subdicts(Lost_subdict_positions, Still_running_enodes_count),
	Diff = Still_running_enodes_count - length(Temp),
	if
		Diff > 0 ->
			Created_splits = utils:pad_list(Temp, Diff);
		true ->
			Created_splits = Temp
	end,
	log("[Master ~p] Created_splits: ~w~n", [self(), Created_splits], false),
	
	{L1, L2} = utils:pad_lists(Split_lost_dictionary, Enodes_alive),
	Attributed_subdictionaries = lists:zip(L1, L2),
	[Id_current_enode ! {new_task, Current_dict, utils:compress(New_positions)} || {{Current_dict, {Id_current_enode, _}}, New_positions} <- utils:zip(Attributed_subdictionaries, Created_splits)],
	[log("[Master ~p] Sends current_dict: ~p to: ~p~n", [self(), Current_dict, Id_current_enode]) || {Current_dict, {Id_current_enode, _}} <- Attributed_subdictionaries],
	Updated_responsive_enodes_list = [{Pid, Positions ++ New_positions} || {New_positions, {Pid, Positions}} <- utils:zip(Created_splits, Enodes_alive)],
	distribute_subdicts(Tail_zombies, Dict_segments, Updated_responsive_enodes_list).

%% @doc upon reception of results, check if the sender is a running enode or a zombie enode (considered dead)
handle_result(false, Enodes_unknown, Dict_segments, Hash_Function, Enodes_all, {Pid, _}, _, Last_sent, Timeout_Multiplier, Master_TS) ->
	%% @doc zombie enode, ignore the result
	log("[Master ~p] Refused answer from zombie enode ~p~n", [self(),Pid]),
	receive_results(Enodes_unknown, Dict_segments, Hash_Function, Enodes_all, Last_sent, Timeout_Multiplier, Master_TS);
handle_result(true, Enodes_unknown, Dict_segments, Hash_Function, Enodes_all, {Pid, Result_list}, Positions, {Last_Segment_Sent, Last_Positions_Sent}, Timeout_Multiplier, Master_TS) ->
	%% @doc accepting results
	log("[Master ~p] Accepted message from ~p: ~p~n", [self(),Pid, Result_list], false),
	save_list(Result_list),

	%% @doc remove enode from remaining enodes list
	New_dict = utils:elem(Last_Segment_Sent + 1, Dict_segments),
	if
		length(New_dict) =:= 0 ->
			% Pid ! exit;
			Remaining_enodes_list_updated = Enodes_unknown;
		true ->
			New_positions = utils:range(Last_Positions_Sent + 1, length(New_dict) + Last_Positions_Sent + 1),
			CNew_Positions = utils:compress(New_positions),
			Pid ! {new_task, New_dict, CNew_Positions},
			Remaining_enodes_list_updated = add_positions(Pid, Enodes_unknown, New_positions)
	end,
	
	Remaining_enodes_list_updated2 = remove_results(Pid, Remaining_enodes_list_updated, Positions),

	[tuple_space:in(Master_TS, {Result_user_name, any}) || {Result_user_name, _} <- Result_list],

	% log("[Master ~p] Accepted answer from ~p, currently working on ~p words~n", [self(), Pid, count_entries(Remaining_enodes_list_updated2)]),
	log("[Master ~p] Size Master_TS is now: ~p~n", [self(), tuple_space:get_TS_size(Master_TS)]),

	[{Pid, Result_list} | receive_results(Remaining_enodes_list_updated2, Dict_segments, Hash_Function, Enodes_all, {Last_Segment_Sent + 1, Last_Positions_Sent + length(New_dict)}, Timeout_Multiplier, Master_TS)].

%% @doc write the provided result list to the outfile results.hrl
save_list([]) -> [];
save_list([Head | Tail]) ->
	{User, Password} = Head,
	file:write_file("out/results.hrl", io_lib:fwrite("~s,~s~n", [User, Password]), [append]),
	save_list(Tail).

%% @doc addthe the position list Positions to the enode's position list with the provided Pid. Leaves the other enodes unchanged in the list
add_positions(_, [], _) -> [];
add_positions(Pid, [{Enode_H_PID, Enode_H_Positions} | Enodes_T], Positions) when Pid =:= Enode_H_PID ->
    Updated_H = {Enode_H_PID, Enode_H_Positions ++ Positions},
    [Updated_H | Enodes_T];
add_positions(Pid, [Enodes_H | Enodes_T], Positions) ->
	[Enodes_H | add_positions(Pid, Enodes_T, Positions)].

%% @doc remove the positions list Positions from the enode's position list with the provided Pid. Leaves the other enodes unchanged in the list
remove_results(_, [], _) -> [];
remove_results(Pid, [{Enode_H_PID, Enode_H_Positions} | Enodes_T], Positions) when Pid =:= Enode_H_PID ->
	Remaining_Positions = lists:subtract(Enode_H_Positions, Positions),
	if    length(Remaining_Positions) > 0 ->
		    Updated_H = {Enode_H_PID, Remaining_Positions},
		    [Updated_H | Enodes_T];
	      true ->
	      	Enodes_T
	end;
remove_results(Pid, [Enodes_H | Enodes_T], Positions) ->
	[Enodes_H | remove_results(Pid, Enodes_T, Positions)].

%% @doc remove idle enodes from list
remove_idle([]) -> [];
remove_idle([{_, []} | Enodes_Tail]) ->
	remove_idle(Enodes_Tail);
remove_idle([Head | Enodes_Tail]) ->
	[Head | remove_idle(Enodes_Tail)].

% %% @doc count words currently treated by the enodes
% count_entries([]) -> 0;
% count_entries([Head | Tail]) ->
% 	{_, Positions} = Head,
% 	length(Positions) + count_entries(Tail).
