-module(subprocess).
-export([start/4]).

%% @doc provides a logging functionality. Takes the same arguments as io:format. If the atom false is provided, logs are only writtent to the .hrl file and not shown in the terminal
log(String, List) -> 
	Filename = Filename = "out/subs/subprocess_" ++ pid_to_list(self()) ++ ".hrl",
	file:write_file(Filename, io_lib:fwrite(String, List), [append]).

start(Subdictionary, Hash_Function, Pid_Master, TS) -> 
	log("[Subprocess ~p] Created with dic ~p~n", [self(), Subdictionary]),
	
	% random:seed(now()),
	% Random_choice = random:uniform(10),
	% log("Random_choice : ~p~n", [Random_choice]),
	% case Random_choice =:= 10 of
	% 	true -> log("[Subprocess ~p] crashed~n", [self()]),
	% 			exit(exit);
	% 	false -> def
	% end,

	Res = find_matches(Subdictionary, Hash_Function, TS),
	Res_cleaned = lists:flatten(Res),
	Pid_Master ! {self(), Res_cleaned},
	log("[Subprocess ~p] Finished and terminates~n", [self()]).

%% @doc request matches from a tuple space
find_matches(Dictionary, Hash_Function, TS) ->
	[lookup_match(Dict_Entry, Hash_Function, TS) || Dict_Entry <- Dictionary].

%% @doc search for the dictionary entry in the tuple space
lookup_match(Dict_Entry, all, TS) ->
	lookup_match(Dict_Entry, md5, TS) ++ lookup_match(Dict_Entry, sha1, TS) ++ lookup_match(Dict_Entry, sha256, TS);
lookup_match(Dict_Entry, Hash_Function, TS) ->
	check_for_messages(),
	Hashed_Dict_Entry = utils:hash(Dict_Entry, Hash_Function),
	Res = tuple_space:in(TS, {any, Hashed_Dict_Entry}),
	case Res =:= null of
		true -> [];
		false -> [answer(Res, Dict_Entry)] ++ lookup_match(Dict_Entry, Hash_Function, TS)
	end.

answer({L , _R}, Dict_Entry) -> {L, Dict_Entry}.
%% @doc regularily check if the message considered_dead has been received, in which case, terminate execution
check_for_messages() ->
	receive
		considered_dead ->	
			log("[Subprocess ~p] Received exit and terminates~n", [self()]),
			exit(exit)
	after 0 -> ok
	end.