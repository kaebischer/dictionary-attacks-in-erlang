% https://github.com/Tarrasch/Tuplespace
-module(tuple_space). 
-export([new/0, in/2, out/2, ts/2, initialize_tuple_space/0, get_TS_size/1, copy_TS/1]). 

log(String, List)-> 
    file:write_file("out/tuples/tuple.hrl", io_lib:fwrite(String, List), [append]).


%% @doc A function returning the pid of a new tuplespace
new() ->
    log("[Tuple space ~p] Spawned~n", [self()]),
    spawn_link(fun ts/0).

%% @doc A function retrieving tuple matching pattern from tuplespace (non-blocking in)
in(TS, Pattern) ->
    Ref = make_ref(),
    TS ! {self(), Ref, Pattern},
    receive
        {Ref, Result} ->
            Result
    end.
    
%% @doc A function putting tuple in tuplespace
out(TS,Tuple) ->
    TS ! {Tuple}.
    
%% @doc Mmain function to deal with in/out tuplespace action.
%% in action will retrieve matching pattern from tuplespace or 
%% it will return null if no matching was found.
ts() -> ts([], []).
ts(Tuples, WaitingList) ->
    receive 
        % handle in messages
        {From, Ref, Pattern} ->
            case recursive_match(Pattern, Tuples, []) of
                {FoundTuple, NewTuples} -> 
                    From ! {Ref, FoundTuple},
                    ts(NewTuples, WaitingList);
                % if no match is found return null
                false -> 
                    From ! {Ref, null},
                    ts(Tuples, WaitingList)
            end;
        % handle out messages
        {Tuple} ->
            case match_list(Tuple, WaitingList, []) of
                false ->
                    ts([Tuple|Tuples], WaitingList);
                {Waiting, List}  ->
                    {From, Ref, _} = Waiting,
                    From ! {Ref, Tuple},
                    ts(Tuples, List)
            end;
        reset ->
            log("[Tuple space ~p] Received reset and terminates~n", [self()]);

        {From, get_size} ->
            log("[Tuple space ~p] Get size~n", [self()]),
            From ! {size, length(Tuples)},
            ts(Tuples, WaitingList);

        {From, copy} ->
            log("[Tuple space ~p] Copy~n", [self()]),
            
            Copy = tuple_space:new(),
            [out(Copy, Tuple) || Tuple <- Tuples],

            From ! {copy, Copy},
            ts(Tuples, WaitingList)
    end.

%% @doc A function matching tuple against waiting list.
% returns false if no match is found, else a tuple with the
% information about the waiting process and the new list.
match_list(_, [], _) ->
    false;
match_list(Tuple, [Head|Tail], NewList) ->
    {_, _, Pattern} = Head,
    case match(Pattern, Tuple) of
        true -> {Head, NewList ++ Tail};
        false -> match_list(Tuple, Tail, [Head | NewList])
    end.

%% @doc A recursive matching function returning matched tuple and new tuples list
recursive_match(_, [], _)->
    false;
recursive_match(Pattern, Tuples, NewTuples) ->
    [Head|Tail] = Tuples,
    case match(Pattern, Head) of
        true -> {Head, NewTuples ++ Tail};
        false -> recursive_match(Pattern, Tail, [Head|NewTuples])
    end.
    
%% @doc A function matching pattern with tuple
match(any,_) -> true;
% make sure P and Q are tuples
match(P,Q) when is_tuple(P), is_tuple(Q) -> 
    match(tuple_to_list(P),tuple_to_list(Q));
match([P|PS],[L|LS]) -> 
    case match(P,L) of
        true -> match(PS,LS); 
        false -> false
    end;
match(P,P) -> true;
match(_,_) -> false.

initialize_tuple_space() ->
    TS = tuple_space:new(),
    link(TS),
    Content = utils:read_file("dictionaries/generated_users"),
    Tokenized_Content = utils:tokenize(Content),
    Tokenized_Tuples = [{Username, Pw} || [Username, Pw] <- Tokenized_Content],
    goto_tuple_space(TS, Tokenized_Tuples),
    TS.

%% @doc send a list of elements to the tuple space
goto_tuple_space(_TS, []) -> ok;
goto_tuple_space(TS, [Head | Tail]) ->
    tuple_space:out(TS,Head),
    goto_tuple_space(TS, Tail).

%% @doc returns the number of remaining elements in the tuple space
get_TS_size(TS) ->
    TS ! {self(), get_size},
    receive 
        {size, Size} -> Size
    end.

%% @doc return a copy of a tuple space
copy_TS(TS) ->
    TS  ! {self(), copy},
    receive
        {copy, Copy} -> Copy
    end.