-module(utils).
-export([ceiling/1, split/2, read_file/1, md5/1, sha1/1, sha256/1, flatten_results_with_pid/2, flatten_results/2]).
-export([pretty_print/1, sort_list/1, elem/2, remove_empty_sublists/1, hash/2, remove_enode/2]).
-export([is_enode_in_list/2, get_split_dict_positions/3, id_not_in_tuple_list/2, get_n_entries_from/3, flatten_once/1]).
-export([pad_lists/2, zip/2, get_enode/2, range/1, range/2, compress/1, decompress/1, first_elements/2, tokenize/1]).
-export([begin_statistics/0, end_statistics/0, pad_list/2]).

%% @doc A function obtaining a ceiling of value.
ceiling(Value) ->
	T = trunc(Value),
	case Value - T == 0 of
	    true -> T;
	    false -> T + 1
	end.

%% @doc A function spliting a list into sublists of length N.
split([], _) -> [];
split(List, N) when N > length(List) -> [List];
split(List, N) ->
	{Head, Tail} = lists:split(N, List),
	[Head | split(Tail, N)].

%% @doc A function reading file and returning a list with its content.
read_file(File_Name) ->
  	{ok, Binary} = file:read_file(File_Name),
  	string:tokens(erlang:binary_to_list(Binary), "\n").

%% @doc use the provided atom Hash_Function as a function name
hash(String, Hash_Function) -> utils:Hash_Function(String).

%% @doc A function hashing string using MD5 method
md5(String) ->
	Md5_Binary = crypto:hash(md5, String),
	hexstring128(Md5_Binary).

%% @doc A function hashing string using SHA-1 method
sha1(String) ->
	Sha1_Binary = crypto:hash(sha, String),
	hexstring160(Sha1_Binary).

%% @doc A function hashing string using SHA-256 method
sha256(String) ->
	Sha256_Binary = crypto:hash(sha256, String),
	hexstring256(Sha256_Binary).
%% @doc bit conversion
hexstring128(<<X:128/big-unsigned-integer>>) -> lists:flatten(io_lib:format("~32.16.0b", [X])).
%% @doc bit conversion
hexstring160(<<X:160/big-unsigned-integer>>) -> lists:flatten(io_lib:format("~40.16.0b", [X])).
%% @doc bit conversion
hexstring256(<<X:256/big-unsigned-integer>>) -> lists:flatten(io_lib:format("~64.16.0b", [X])).

%% @doc A function flattening results with Pids for debugging
flatten_results_with_pid([], Flatten_Results) -> Flatten_Results;
flatten_results_with_pid([{Pid, Res} | List], Flatten_Results) ->
	Flat_Res = lists:flatten(Res),
	flatten_results_with_pid(List, [{Pid, Flat_Res} | Flatten_Results]).

%% @doc A function flattening results without Pids
flatten_results([], Flatten_Results) -> lists:concat(Flatten_Results);
flatten_results([{_, Res} | List], Flatten_Results) ->
	Flat_Res = lists:flatten(Res),
	flatten_results(List, [Flat_Res | Flatten_Results]).

%% @doc A function printing results 
pretty_print([]) -> ok;
pretty_print([Head | Tail]) ->
	io:fwrite("~p~n", [Head]),
	pretty_print(Tail).

%% @doc A function sorting results in alphanumerical order
sort_list([]) -> [];
sort_list(List) -> lists:sort(fun({V, _}, {W, _}) -> V =< W end , List).

%% @doc get the element at position Position in the list
elem(_, []) -> [];
elem(0, [Head | _]) -> Head;
elem(Position, [_ | Tail]) ->
	elem(Position - 1, Tail).

%% @doc remove empty elements which are emtpy lists from a list
remove_empty_sublists(List) ->	[Element || Element <- List, length(Element) > 0].

%% @doc removes the enodes with the provided pid
remove_enode(_, []) -> [];
remove_enode(Element, [{Pid, _} | Tail]) when Element =:= Pid -> Tail;
remove_enode(Element, [Head | Tail]) ->
	[Head | remove_enode(Element, Tail)].

%% @doc returns whether an enode with the provided pid is in the lsit
is_enode_in_list(_, []) ->	false;
is_enode_in_list(Pid, [Head | Tail]) ->
	{Current_Pid, _} = Head,
	(Pid =:= Current_Pid) or is_enode_in_list(Pid, Tail).

%% @doc get a split of the positions of the the dictionary entries
get_split_dict_positions(Len_tot, Start, Len_sub) when Len_tot >= Start + Len_sub ->
	[{Start, Len_sub} | get_split_dict_positions(Len_tot, Start + Len_sub, Len_sub)];
get_split_dict_positions(Len_tot, Start, _) ->
	[{Start, Len_tot - Start}].

%% @doc return true if an id has not been found in a list, false otherwise
id_not_in_tuple_list(_, []) -> true;
id_not_in_tuple_list(Id, [{Head_id, _} | _]) when Id =:= Head_id -> false;
id_not_in_tuple_list(Id, [_ | Tail]) -> id_not_in_tuple_list(Id, Tail).

%% @doc return n entries of a list, starting from position From
get_n_entries_from(0, _, _) -> [];
get_n_entries_from(N, From, List) ->
	[utils:elem(From, List) | get_n_entries_from(N - 1, From + 1, List)].

%% @doc applies one level of flattening to a list
flatten_once([]) -> [];
flatten_once([H | T]) ->
	H ++ flatten_once(T).

%% @doc pads the shorter lists with empty sublists until both lists have the same length
pad_lists(L1, L2) when length(L1) > length(L2) ->
	pad_lists(L1, [[] | L2]);
pad_lists(L1, L2) when length(L1) < length(L2) ->
	pad_lists([[] | L1], L2);
pad_lists(L1, L2) -> {L1, L2}.

%% @doc zip lists of unequal length
zip([], _) -> [];
zip(_, []) -> [];
zip([H1 | T1], [H2 | T2]) ->
	[{H1, H2} | zip(T1, T2)].

%% @doc returns the enode and its position corresponding to the provided pid
get_enode(_, []) -> {};
get_enode(Element, [{Pid, Positions} | _]) when Element =:= Pid -> {Pid, Positions};
get_enode(Element, [_ | Tail]) ->
	get_enode(Element, Tail).

% doc returns the list of integers [0, N[
range(N) ->
	range(0, N).
% doc returns the list of integers [Start, End[
range(Start, End) when Start =:= End -> [];
range(Start, End) ->
	[Start | range(Start + 1, End)].

%% @doc apply the compression for lists of sequentialy increasing integer
compress([]) -> [];
compress([Head | Tail]) ->
	{Last_in_sequence, Remaining} = last_in_sequence_and_remaining(Head, Tail),
	[{Head, Last_in_sequence} | compress(Remaining)];
compress(_) -> [].

%% @doc find the last value in the current sequence and return it with the rest of the lsit
last_in_sequence_and_remaining(Last_Val, [Head | Tail]) when Last_Val + 1 =:= Head ->
	last_in_sequence_and_remaining(Head, Tail);
last_in_sequence_and_remaining(Last_Val, Remaining) -> {Last_Val, Remaining}.

%% @doc decompress the list of tuples compressed with the function above
decompress([]) -> [];
decompress([{Start, End} | Tail]) ->
	utils:range(Start, End + 1) ++ decompress(Tail);
decompress(_) -> [].

%% @doc return the first n elements of a list
first_elements(0, _) -> [];
first_elements(_, []) -> [];
first_elements(N, [Head | Tail]) ->
	[Head | first_elements(N-1, Tail)].

%% @doc split a list with a comma seperator
tokenize([]) -> [];
tokenize([Head | Tail]) ->
    [string:tokens(Head, ",") | tokenize(Tail)].

%% @doc A function begining calculation runtime and wall_clock for performance analysis
begin_statistics() ->
	statistics(runtime),
    statistics(wall_clock).

%% @doc A function ending calculation runtime and wall_clock for performance analysis and printing the result
end_statistics() ->
	{_, Time1} = statistics(runtime),		%---- time elapsed during user code and during kernel code
    {_, Time2} = statistics(wall_clock),	%---- time elapsed between the start of the process and 'now'
    io:format("~nTime processing :~n------------~n -runtime = ~p milliseconds~n -wall_clock = ~p milliseconds~n", [Time1, Time2]).

%% @doc prepend n emtpy sublists to a list
pad_list(List, 0) ->
	List;
pad_list(List, N) ->
	[[] | pad_list(List, N - 1)].